#!/bin/bash
#1-this .bash script runs and stops iperf application
#for test_duration

#2-this script opens single port, which is a command input

#usage: ./run.sh 192.168.1.33 3 2 5001 

#ex:test_duration = 50; run_interval=3; pause_interval=2;
#iperf runs in t=[0..3], t=[5...8], ..., t=[47,50]

if [ "$#" -ne 4 ];
	then
	echo "illegal usage"
	echo "./run.sh <destination_ip> <run_interval> <pause_interval> <port_number>"
	exit 1
fi

destination_ip=$1
run_interval=$2;
pause_interval=$3;
port_number=$4;

while true;
	do
	iperf -c $destination_ip -p $port_number -t $run_interval -i1 &
	iperf_pid=$!
	sleep $(($run_interval))
	kill $iperf_pid
	sleep $(($pause_interval))
done 
