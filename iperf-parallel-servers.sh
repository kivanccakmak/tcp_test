#!/bin/bash
# Run multiple parallel instances of iperf servers

if [ "$#" -ne 2 ];
	then
	echo "illegal usage"
	echo "./iperf-parallel-servers.sh <base_port> <port_quantity>"
	exit 1
fi

# Assumes the port numbers used by the servers start at 5001 and increase
# e.g. 5001, 5002, 5003, ...
base_port=$1

# Command line input: number of servers
# E.g. 5
num_servers=$2
shift

# Optional command line input: other iperf options
# E.g. -u
iperf_options="$*"
report_base="port"
# Run iperf multiple times
for i in `seq 1 $num_servers`; do

	# Set server port
	server_port=$(($base_port+$i));

	# Report file includes server port
	report_file=${report_base}-${server_port}.txt
	# Run iperf
	iperf -s -p $server_port $iperf_options -i1 &> $report_file &

done


