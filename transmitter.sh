#!/bin/bash
#1-this bash enables usage of dummy packet construction application [iperf] and 
#loadable linux kernel module [tcp_probe] which provides instantenous congestion window sizes
#2-consequently, outputs of congestion window and iperf application is provided systematically
#to directories in .txt and .out files  
base_port=5000
test_duration=5
#./transmitter.sh <window_type> <interference> <port_quantity> <destination_ip> 
if [ "$#" -ne 4 ];
	then
	echo "illegal usage"
	echo "./transmitter.sh <window_type> <interference> <port_quantity> <destination_ip>"
	echo "example with interference: ./transmitter.sh cubic interference 4 192.168.2.1"
	echo "example without interference: ./transmitter.sh cubic non 4 192.168.2.1"
	exit 1
fi
window_type=$1 #first positional variable is tcp_congestion_control algorithm
#=====================================================================================================================================
#RECORD NAMING CONVENTION
#=====================================================================================================================================
interference=$2 #second positional variable denotes whether simulation is performed under interference or not
		#this variable is used for only name recoding convention 
		#names of sub directories, .txt files and .out files depend on this variable(but nothing else in principles of code!)
if [ "$interference" == "interference" ];
then
	sub_directory_record_begin="Interference_P"
	sub_iperf_record_begin="$window_type""__Int__THR_P"
	sub_probe_record_begin="$window_type""_CW__Int__P"
	echo "interference network exists"
else
	sub_directory_record_begin="NoInterference_P"
	sub_iperf_record_begin="$window_type""_THR_P"
	sub_probe_record_begin="$window_type""_CW_P"
	echo "interference network does not exist"
fi
#=====================================================================================================================================
echo "window type is changing to $window_type";
echo $window_type > /proc/sys/net/ipv4/tcp_congestion_control;
mkdir ${window_type};
echo "$window_type directory is created"
#=====================================================================================================================================
dir_name="$window_type""/""$dir_name"
port_quantity=$3 #third positional variable is number of ports, experiment would be done 
		 #for port_number = [1,2,..port_quantity]

echo "port quantity is equal to $port_quantity";
destination_ip=$4 #fourth positional variable is destination ip address
echo "#======================================================================================================================================"
for((counter=1;counter<=$port_quantity;counter++))
do
	modprobe -r tcp_probe			#STEP 1:tcp_probe initialization
	modprobe tcp_probe [port=] full=1
	chmod 444 /proc/net/tcpprobe
	record_directory="$sub_directory_record_begin""$counter" #STEP 2: record file names are given
	record_directory="$window_type""/""$record_directory"
	mkdir ${record_directory}
	echo "$record_directory directory is created" 
	prob_record_file="$record_directory""/""$sub_probe_record_begin""$counter"".out";
	echo "tcp_probe record file = $prob_record_file"
	cat /proc/net/tcpprobe > $prob_record_file &
	TCPCAP=$!
	echo "ready to start simulation"
	for((port_counter=1; port_counter<=$counter; port_counter++))
	do      
		PORT_NUMBER=$(($base_port+$port_counter));
		iperf_report_file="$record_directory""/""$sub_iperf_record_begin""$port_counter""_port""$PORT_NUMBER"".txt";
		echo "CURRENT PORT = $PORT_NUMBER";
		echo "iperf report file = $iperf_report_file"
		iperf -c $destination_ip -p $PORT_NUMBER -t $test_duration -i1 &> $iperf_report_file &
		iperf_pid[$port_counter]=$! 
	done
	sleep $(($test_duration))
	for((port_counter=1; port_counter<=$counter; port_counter++))
	do
		kill ${iperf_pid[$port_counter]}
		unset iperf_pid[$port_counter]}
	done
	sudo kill $TCPCAP
done
