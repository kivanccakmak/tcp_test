#tcp-test
------------
This repository is created to observe TCP's instantenous congestion
window size with loadable linux kernel module **_tcpprobe_** and data construction
application **_iperf_**.

Below, I provide how to use said codes: 

Before running bash files, first become a root and change permissions as following:

```
sudo chmod u+x receiver.sh;
sudo chmod u+x transmitter.sh;
```

Here, parantheses are used for ID of PC

```
(1) ./receiver.sh <port_quantity>
(1) ./receiver.sh 2
```

```
(2) ./transmitter.sh <window_type> <interference> <port_quantity> <destination_ip>
(2) ./transmitter.sh cubic non 2 192.168.2.33
```

Followingly, outputs would be recorded to directories in a systematic manner.
.out files record instantenous window sizes(7th column of them) and .txt files record
throughput of each socket

In order to obtain data mining from this .txt and .out files in a scalable manner,
first create a directory "TCP_Resuls" and put outputs of each window into them (such as, cubic, reno etc.)

Afterwards, run "main()" with Octave -or MATLAB- to obtain .mat files and .eps figures in a systematic manner

In order to obtain instantenous congestion window size and throughput within interference, I also provide
simple interference creating .bash files. Here, you need to have third and fourth devides that are communicating 
on same channel.

Similarly, first you need to change permissions of .bash files

```
sudo chmod u+x run.sh
sudo chmod u+x iperf-parallel-servers.sh
```

```
(3) ./iperf-parallel-servers.sh ./iperf-parallel-servers.sh <base_port> <port_quantity>
(3) ./iperf-parallel-servers 5000 1
(4) ./run.sh <destination_ip> <run_interval> <pause_interval> <port_number>"
(4) ./run.sh 192.168.2.35 3 2 5001
```

So, third PC listens port 5000+1 = 5001. Consequently, fourth pc starts iperf
connection on port 5001 with two parameters. 

run_interval: iperf runs for that much seconds 
pause_interval: iperf stops for that much seconds
 
But both of runs with infinite loop.
In order to kill them, use console as following:


```
pgrep iperf
ps aux
```
this will show running iperf commands
```
pkill iperf
```
you can check with ps aux again etc.

So, if you have created interference with 3rd and 4th PC's,

you would better to call ./transmitter.sh as following:

```
(2) ./transmitter.sh cubic interference 2 192.168.2.33
```

INSTEAD OF

```
(2) ./transmitter.sh cubic non 2 192.168.2.33
```

The reason is, second command line variable (which become interference now)
would provide naming convention of interference in all directories 
and files which points interference exist in network.

